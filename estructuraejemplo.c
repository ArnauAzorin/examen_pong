/*******************************************************************************************
*
*   raylib game: FINAL PONG - game template
*
*   developed by [Arnau Azorin]
*
*   This example has been created using raylib 1.0 (www.raylib.com)
*   raylib is licensed under an unmodified zlib/libpng license (View raylib.h for details)
*
*   Copyright (c) 2014 Ramon Santamaria (Ray San)
*
********************************************************************************************/

#include "raylib.h"

typedef enum GameScreen { LOGO, TITLE, GAMEPLAY, ENDING } GameScreen;

int main()
{
    // Initialization
    //--------------------------------------------------------------------------------------
    int screenWidth = 800;
    int screenHeight = 450;
    char windowTitle[30] = "raylib game - FINAL PONG";
    InitWindow(screenWidth, screenHeight, windowTitle);
    
    GameScreen screen = LOGO;
    
    // TODO: Define required variables here..........................(0.5p)
    // NOTE: Here there are some useful variables (should be initialized)
    
    //LOGO
    Texture2D logo2 = LoadTexture("resources/logo2.png");
    float alpha = 0.0f;
    bool fadeIn = true;
    Rectangle rec = {0, 0, screenWidth, screenHeight};
    Color recColor = BLACK;
    
    //TITLE
    Texture2D logo = LoadTexture("resources/logo1.png");
    Texture2D title = LoadTexture("resources/title.png");
    char startText[16] = "PRESS ENTER";
    Vector2 titlePos = {screenWidth/2 - logo.width/2, screenHeight - logo.height};
    bool  blink = false;
    InitAudioDevice();
    Music music = LoadMusicStream("resources/j.ogg");
    
    
    //GAMEPLAY
    Color ballColor = {30, 149, 216, 255};
    Color playerColor = {6, 170, 55, 255};
    Color enemyColor = {90, 90, 90, 255};
    Color playerFillColor = {133, 230, 152, 255};
    Color enemyFillColor = {139, 139, 139, 255};
    
    float topMargin = 50;
    float bgMargin = 8;
    float fielMargin = 5;
    float counterMargin = 45;
    float lifeMargin = bgMargin;
    
    
    
    Rectangle bgRec = {0, topMargin, screenWidth, screenHeight - topMargin};
    Rectangle fielRec = {bgRec.x + bgMargin, bgRec.y + bgMargin, bgRec.width - bgMargin*2, bgRec.height - bgMargin*2};
    Texture2D fondo = LoadTexture("resources/fondo.png");
    
    Rectangle playerBgRec = {0,0, screenWidth/2 - counterMargin, topMargin };
    Rectangle playerFillRec = {playerBgRec.x + lifeMargin, playerBgRec.y + lifeMargin, playerBgRec.width - lifeMargin*2, playerBgRec.height - lifeMargin*2};
    Rectangle playerLifeRec = playerFillRec;
    
    Rectangle enemyBgRec = {screenWidth/2 + counterMargin, 0, screenWidth/2 - counterMargin, topMargin };
    Rectangle enemyFillRec = {enemyBgRec.x + lifeMargin, enemyBgRec.y + lifeMargin, enemyBgRec.width- lifeMargin*2, enemyBgRec.height - lifeMargin*2};
    Rectangle enemyLifeRec = enemyFillRec;
    
    Rectangle player = {fielRec.x + fielMargin, fielRec.y + fielRec.height/2 -40, 20, 80};
    int playerSpeedY = 4;
    
    Rectangle enemy= {fielRec.x + fielRec.width - fielMargin - 20, fielRec.y + fielRec.height/2 -40, 20, 80};
    int enemySpeedY = 4;
    float AImargin = enemy.height/4;
    
    Vector2 ballPosition = {fielRec.x + fielRec.width/2, fielRec.y + fielRec.height/2};
    Vector2 ballSpeed = {GetRandomValue(4,5), GetRandomValue(3,5)};
    if(GetRandomValue(0,1)) ballSpeed.x *= -1;
    if(GetRandomValue(0,1)) ballSpeed.y *= -1;
    
    int ballRadius = 20;
    
    int playerLife = 5;
    int enemyLife = 5;
    
    float widthDamage = playerLifeRec.width/playerLife;
    
    int secondsCounter = 99;
    
    int framesCounter = 0;          // General pourpose frames counter
    
    int gameResult = -1;        // 0 - Loose, 1 - Win, 2 - Draw,  -1 - Not defined
    
    bool pause = false;
    
    //ENDING
    
    Music musicEnd = LoadMusicStream("resources/a.ogg");
    Texture2D end = LoadTexture("resources/astral.png");
    Music lost = LoadMusicStream("resources/lost.ogg");
    Music again = LoadMusicStream("resources/c.ogg");
    // NOTE: If using textures, declare Texture2D variables here (after InitWindow)
    // NOTE: If using SpriteFonts, declare SpriteFont variables here (after InitWindow)
    
    // NOTE: If using sound or music, InitAudioDevice() and load Sound variables here (after InitAudioDevice)
    
    SetTargetFPS(60);
    //--------------------------------------------------------------------------------------
    
    // Main game loop
    while (!WindowShouldClose())    // Detect window close button or ESC key
    {
        // Update
        UpdateMusicStream(music);
        UpdateMusicStream(musicEnd);
        UpdateMusicStream(lost);
        UpdateMusicStream(again);
        //----------------------------------------------------------------------------------
        switch(screen) 
        {
            case LOGO: 
            {
                // Update LOGO screen data here!
                
                // TODO: Logo fadeIn and fadeOut logic...............(0.5p)
                if (fadeIn)
                {
                    alpha += 1.0f/90;
                    
                    if (alpha >= 1.0f)
                    {
                        alpha = 1.0f;
                        
                        framesCounter++;
                        if(framesCounter % 120 == 0)
                        {
                            framesCounter = 0;
                            fadeIn = false; 
                        }
                    }
                }
                else
                {
                    alpha -= 1.0f/90;
                    
                    if (alpha <= 0.0f)
                    {
                        //TITLESCREEN
                        framesCounter = 0;
                        screen = TITLE;
                    }
                }
                
                if (IsKeyPressed(KEY_ENTER))
                {
                    framesCounter = 0;
                    screen = TITLE;
                }
            } break;
            case TITLE: 
            {
                // Update TITLE screen data here!
                
                // TODO: Title animation logic.......................(0.5p)
                fadeIn = true;
                if (fadeIn)
                {
                    alpha += 1.0f/240;
                    
                    if (alpha >= 1.0f)
                    {
                        alpha = 1.0f;
                        
                        framesCounter++;
                        if(framesCounter % 240 == 0)
                        {
                            framesCounter = 0;
                            fadeIn = false; 
                        }
                    }
                }
                
                if (titlePos.y < screenHeight/2 - logo.height/2)
                {
                    titlePos.y += 8;
                    PlayMusicStream(music);
                    if (IsKeyPressed(KEY_ENTER))
                    {
                        titlePos.y = 100;
                    }
                }
                else 
                {
                    titlePos.y = screenHeight/2 - logo.height/2;
                    // TODO: "PRESS ENTER" logic.........................(0.5p)
                        framesCounter++;
                        if (framesCounter % 12 == 0)
                        {
                            framesCounter = 0;
                            blink = !blink;
                        }
                        if (IsKeyPressed(KEY_ENTER))
                        {
                            framesCounter = 0;
                            screen = GAMEPLAY;
                        }
                }
                
                
            } break;
            case GAMEPLAY:
            { 
                // Update GAMEPLAY screen data here!
                
                 if (!pause)
                {
                     // TODO: Ball movement logic.........................(0.2p)
                    if(ballSpeed.x > 10) ballSpeed.x = 10;
                    if(ballSpeed.y > 10) ballSpeed.y = 10;
                    ballPosition.x += ballSpeed.x;
                    ballPosition.y += ballSpeed.y;
                    // TODO: Player movement logic.......................(0.2p)
                    if (IsKeyDown(KEY_UP)) player.y -= playerSpeedY;
                    if (IsKeyDown(KEY_DOWN)) player.y += playerSpeedY;
                    
                    if (player.y < fielRec.y) player.y = fielRec.y;
                    if (player.y > fielRec.y + fielRec.height - player.height) player.y = fielRec.y + fielRec.height - player.height;
                    // TODO: Enemy movement logic (IA)...................(1p)
                    if ((ballPosition.x > fielRec.x + fielRec.width/2) && ballSpeed.x > 0)
                    {
                        if(ballPosition.y < enemy.y + enemy.height/2 - AImargin) enemy.y -= enemySpeedY;
                        if(ballPosition.y > enemy.y + enemy.height/2 + AImargin) enemy.y += enemySpeedY;
                    }
                    if (enemy.y < fielRec.y) enemy.y = fielRec.y;
                    if (enemy.y > fielRec.y + fielRec.height - enemy.height) enemy.y = fielRec.y + fielRec.height - enemy.height;
                    // TODO: Collision detection (ball-player) logic.....(0.5p)
                    if (CheckCollisionCircleRec(ballPosition, ballRadius, player) && ballSpeed.x < 0)
                    {
                        ballSpeed.x *= -1.02;
                    }
                    // TODO: Collision detection (ball-enemy) logic......(0.5p)
                    if (CheckCollisionCircleRec(ballPosition, ballRadius, enemy) && ballSpeed.x > 0)
                    {
                        ballSpeed.x *= -1.02;
                    }
                    // TODO: Collision detection (ball-limits) logic.....(1p)
                        //TOP
                    if ((ballPosition.y - ballRadius < fielRec.y) && ballSpeed.y < 0)
                    {
                        ballSpeed.y *= -1.02;
                    }
                        //BOTTOM
                    if ((ballPosition.y + ballRadius > fielRec.y + fielRec.height) && ballSpeed.y > 0)
                    {
                        ballSpeed.y *= -1.02;
                    }
                        //LEFT
                     if ((ballPosition.x - ballRadius < fielRec.x ) && ballSpeed.x < 0)
                    {
                        ballSpeed.x *= -1;
                        ballSpeed.x = 5;
                        playerLife --;
                        // TODO: Player Life bars decrease logic....................(1p)
                        playerLifeRec.width -= widthDamage;
                    }   
                        //RIGHT
                      if ((ballPosition.x + ballRadius > fielRec.x + fielRec.width) && ballSpeed.x > 0)
                    {
                        ballSpeed.x *= -1;
                        ballSpeed.x = -5;
                        enemyLife--;
                        // TODO: Enemy Life bars decrease logic....................(1p)
                        enemyLifeRec.width -= widthDamage;
                        enemyLifeRec.x += widthDamage;
                    }  
                    

                    // TODO: Time counter logic..........................(0.2p)
                    framesCounter++;
                    if(framesCounter % 60 == 0)
                    {
                        framesCounter = 0;
                        secondsCounter --;
                    }
                    // TODO: Game ending logic...........................(0.2p)
                        if(secondsCounter <=0)
                        {
                            if(playerLife < enemyLife) gameResult = 0;
                            else if(playerLife > enemyLife) gameResult = 1;
                            else gameResult = 2;
                        }
                        else if (playerLife <= 0)gameResult = 0;
                        else if (enemyLife <= 0) gameResult = 1;
                    
                       if (gameResult != -1)
                       {
                           screen = ENDING;
                       }                           
                }                     
                // TODO: Pause button logic..........................(0.2p)
                if (IsKeyPressed(KEY_P))
                {
                    pause = !pause;
                    if (pause)PauseMusicStream(music);
                    else ResumeMusicStream(music);    
                }
            } break;
            case ENDING: 
            {
                // Update END screen data here!
                
                    StopMusicStream (music);
                    StopMusicStream (again);
                    framesCounter++;
                        if (framesCounter % 12 == 0)
                        {
                            framesCounter = 0;
                            blink = !blink;
                        }
                    if(gameResult>=1)PlayMusicStream (musicEnd);
                    else PlayMusicStream (lost);
                                              
                // TODO: Replay / Exit game logic....................(0.5p)
                
                 if (IsKeyPressed(KEY_ENTER))
                    {
                        StopMusicStream(lost);
                        StopMusicStream(musicEnd);
                        PlayMusicStream (again);
                        
                        secondsCounter = 99;
                        framesCounter = 0;
                        
                        ballPosition = (Vector2) {fielRec.x + fielRec.width/2, fielRec.y + fielRec.height/2};
                        ballSpeed = (Vector2) {GetRandomValue(4,5), GetRandomValue(3,5)};
                        if(GetRandomValue(0,1)) ballSpeed.x *= -1;
                        if(GetRandomValue(0,1)) ballSpeed.y *= -1;
                        
                        playerLife = 5;
                        enemyLife = 5;
                        
                        pause = false;
                        
                        gameResult = -1;
                        
                        enemyLifeRec = enemyFillRec;
                        playerLifeRec = playerFillRec;
                        
                        player.y  = fielRec.y + fielRec.height/2 -40;
                        enemy.y = player.y;
                        
                        screen = GAMEPLAY;
                    }
                    
            } break;
            default: break;
        }
        //----------------------------------------------------------------------------------
        
        // Draw
        //----------------------------------------------------------------------------------
        BeginDrawing();
        
            ClearBackground(RAYWHITE);
            
            switch(screen) 
            {
                case LOGO: 
                {
                    // Draw LOGO screen here!
                    
                    // TODO: Draw Logo...............................(0.2p)
                    DrawRectangleRec(rec, recColor);
                    DrawTexture(logo2, screenWidth/2 - logo2.width/2, screenHeight/2 - logo2.height/2, Fade(WHITE, alpha));
                    
                    
                } break;
                case TITLE: 
                {
                    // Draw TITLE screen here!
                    
                    // TODO: Draw Title..............................(0.2p)
                    DrawRectangleRec(rec, recColor);
                    DrawTextureV(logo, titlePos, WHITE);
                    DrawTexture(title, screenWidth/2 - title.width/2, screenHeight/2 - title.height/2, Fade(WHITE, alpha));
                    // TODO: Draw "PRESS ENTER" message..............(0.2p)
                    if (blink)DrawText (startText, screenWidth/2 - MeasureText (startText, 20)/2, screenHeight-50, 20, RED);
                    
                } break;
                case GAMEPLAY:
                { 
                    // Draw GAMEPLAY screen here!
                    DrawRectangleRec(bgRec, BLACK);
                    DrawRectangleRec(fielRec, RAYWHITE);
                    DrawTexture(fondo, screenWidth/2 - fondo.width/2, screenHeight/2 - fondo.height/2, WHITE);
                    DrawLineEx((Vector2){bgRec.x + bgRec.width/2, bgRec.y}, (Vector2){bgRec.x + bgRec.width/2, bgRec.y + bgRec.height}, bgMargin, BLACK);
                    
                    DrawCircleV(ballPosition, ballRadius, ballColor);
                    // TODO: Draw player and enemy...................(0.2p)
                    DrawRectangleRec(player, playerColor);
                    DrawRectangleRec(enemy, enemyColor);
                    // TODO: Draw player and enemy life bars.........(0.5p)
                    DrawRectangleRec(playerBgRec, BLACK);
                    DrawRectangleRec(playerFillRec, playerFillColor);
                    DrawRectangleRec(playerLifeRec, playerColor);
                    DrawRectangleRec(enemyBgRec, BLACK);
                    DrawRectangleRec(enemyFillRec, enemyFillColor);
                    DrawRectangleRec(enemyLifeRec, enemyColor);
                    // TODO: Draw time counter.......................(0.5p)
                    DrawText(FormatText("%i",secondsCounter),screenWidth/2 - MeasureText(FormatText("%i",secondsCounter),40)/2, topMargin - 42, 40, ballColor);
                    // TODO: Draw pause message when required........(0.5p)
                    if (pause)
                    {
                        DrawRectangle(0,0, screenWidth, screenHeight, Fade (WHITE, 0.8f));
                        DrawText("PAUSE",screenWidth/2 - MeasureText("PAUSE", 30)/2, screenHeight/2- 15, 30, BLACK);
                    }
                    
                } break;
                case ENDING: 
                {
                    // Draw END screen here!
                    
                    // TODO: Draw ending message (win or loose or draw)......(0.2p)
                    DrawRectangleRec(rec, recColor);
                    DrawTexture(end, screenWidth/2 - end.width/2, screenHeight/2 - end.height/2, WHITE);
                    if (gameResult == 0 && blink)DrawText("(-_-) YOU LOSE (-_-)", screenWidth/2 - MeasureText("(-_-) YOU LOSE (-_-)", 40)/2, 150 , 40, RED);
                    else if(gameResult ==  1 && blink)DrawText("(/-_-)/ YOU WIN (/-_-)/", screenWidth/2 - MeasureText("(/-_-)/ YOU WIN (/-_-)/", 40)/2, 150 , 40, playerColor);
                    else if (gameResult == 2 && blink)DrawText("_(-.-)_ DRAW GAME _(-.-)_", screenWidth/2 - MeasureText("_(-.-)_ DRAW GAME _(-.-)_", 40)/2, 150 , 40, ORANGE);
                     
                    
                    
                    DrawText("[ENTER] Retry", screenWidth/2 - MeasureText("[ENTER] Retry", 20)/2, screenHeight - 100 , 20, WHITE);
                    DrawText("[ESC] Exit", screenWidth/2 - MeasureText("[ESC] Exit", 20)/2, screenHeight - 75 , 20, WHITE);
                    DrawText("Made by Arnau Azorin", screenWidth/5 - MeasureText("Made by Arnau Azorin", 20)/2, screenHeight - 50 , 20, WHITE);
                } break;
                default: break;
            }
        
            DrawFPS(10, 10);
        
        EndDrawing();
        //----------------------------------------------------------------------------------
    }

    // De-Initialization
    UnloadMusicStream(music);
    UnloadMusicStream(musicEnd);
    UnloadMusicStream(lost);
    UnloadMusicStream(again);
    
    CloseAudioDevice();
    //--------------------------------------------------------------------------------------
    
    // NOTE: Unload any Texture2D or SpriteFont loaded here
    UnloadTexture (logo);
    CloseWindow();        // Close window and OpenGL context
    //--------------------------------------------------------------------------------------
    
    return 0;
}